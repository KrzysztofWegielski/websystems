
var ajaxRequest = function () {
    var ajaxConnector = function (method, id, data) {
        const apiUrl =  '/api/products';
        const ajaxMethod = method  || 'GET';
        let url = apiUrl;
        if (parseInt(id) > 0) {
            url += '/' + id;
        } else if(ajaxMethod !== 'PUT') {
            url += '/list';
        }
        let ajaxObject = {
            url: url,
            type: ajaxMethod,
            dataType: "json",
            async: true
        };
        if (data) {
            ajaxObject['data'] = productData;
        }
        return $.ajax(ajaxObject);
    };
    return {
        getAjax: function(productId) {
            return ajaxConnector('GET', productId);
        },
        putAjax: function (productId, data) {
            return ajaxConnector('PUT', productId, data);
        },
        postAjax: function (data) {
            return ajaxConnector('POST', 0, data);
        },
        deleteAjax: function deleteAjax(productId) {
            return ajaxConnector('DELETE', productId);
        }
    }
};