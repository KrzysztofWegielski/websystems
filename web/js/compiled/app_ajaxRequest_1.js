var ajaxRequest = function () {
    var ajaxConnector = function (method, id, data, additionalAction) {
        const apiUrl =  '/api/products';
        const ajaxMethod = method  || 'GET';
        let url = apiUrl;
        if (parseInt(id) > 0) {
            url += '/' + id;
        } else if(ajaxMethod === 'GET') {
            if(additionalAction) {
                url = '/api/' + additionalAction;
            } else {
                url += '/list';
            }
        }
        let ajaxObject = {
            url: url,
            type: ajaxMethod,
            dataType: "json",
            async: true
        };
        if (data) {
            ajaxObject['data'] = data;
        }
        return $.ajax(ajaxObject);
    };
    return {
        getAjax: function(productId, six) {
            return ajaxConnector('GET', productId, [], six);
        },
        putAjax: function (productId, data) {
            return ajaxConnector('PUT', productId, data);
        },
        postAjax: function (data) {
            return ajaxConnector('POST', 0, data);
        },
        deleteAjax: function deleteAjax(productId) {
            return ajaxConnector('DELETE', productId);
        }
    }
};

function showModal(type, id) {
    if (id) {
        ajaxRequest().getAjax(id).done(function (data) {
            if (type === "EDIT") {
                $('#name').val(data.result.name);
                $('#id').val(data.result.id);
                $('#amount').val(data.result.amount);
                $('#productModal').modal('show');
            } else {
                $('#vname').html(data.result.name);
                $('#vamount').html(data.result.amount);
                $('#productViewModal').modal('show');
            }
        });
    } else {
        $('#name').val('');
        $('#id').val('');
        $('#amount').val('');
        $('#productModal').modal('show');
    }
}

function hideModal(type) {
    $('#name').val('');
    $('#id').val('');
    $('#amount').val('');
    $('#vname').html('');
    $('#vamount').html('');
    $('#productModal').modal('hide');
}

function buildDataTables(data) {
    let list = [];
    $.each(data, function(index, element) {
        list.push([
            element.id, element.name, element.amount,
            '<span class="action">' +
            '<i class="fa fa-search" title="Show" onclick="showModal(\'SHOW\', ' + element.id + ')"></i>' +
            '<i class="fa fa-cog" title="Edit" onclick="showModal(\'EDIT\', ' + element.id + ')"></i>' +
            '<i class="fa fa-trash delete" title="Delete" data-id="' + element.id + '"></i>' +
            '</span>'
        ]);
    });
    return list;
}