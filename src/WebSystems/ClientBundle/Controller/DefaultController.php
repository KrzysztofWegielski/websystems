<?php

namespace WebSystems\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WebSystemsClientBundle:Default:index.html.twig');
    }
}
