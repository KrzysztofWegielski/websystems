<?php

namespace WebSystems\RestBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findMoreThanFive()
    {
        return $this->getEntityManager()->createQuery(
            'SELECT p FROM WebSystemsRestBundle:Product p WHERE p.amount > 5'
        )->getResult();
    }
    public function findZero()
    {
        return $this->getEntityManager()->createQuery(
            'SELECT p FROM WebSystemsRestBundle:Product p WHERE p.amount = 0'
        )->getResult();
    }
}
