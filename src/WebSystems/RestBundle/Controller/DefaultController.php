<?php

namespace WebSystems\RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WebSystemsRestBundle:Default:index.html.twig');
    }
}
