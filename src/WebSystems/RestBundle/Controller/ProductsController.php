<?php

namespace WebSystems\RestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WebSystems\RestBundle\Entity\Product;

class ProductsController extends FOSRestController
{
    const MESSAGE_NOT_FOUND = "Product not found";
    const MESSAGE_ADDED = "Product has been added";
    const MESSAGE_NOT_ACCEPTABLE = 'Field "name" should not be empty and field "amount" should be equal or greater than 0';
    const MESSAGE_UPDATED = "Product has been updated";
    const MESSAGE_DELETED = "Product has been deleted";

    public function getProductAction($id)
    {
        $doctrine = $this->getDoctrine();
        $product = $doctrine->getRepository('WebSystemsRestBundle:Product')->findOneById($id);
        if (!$product) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        }
        $data = $this->createProductData($product);
        return $this->createJsonResponse($data, JsonResponse::HTTP_OK);
    }

    public function fiveAction() {
        $products = $this->getDoctrine()->getRepository('WebSystemsRestBundle:Product')->findMoreThanFive();
        if (empty($products)) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        }
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->createProductData($product);
        }
        return $this->createJsonResponse($data, JsonResponse::HTTP_OK);
    }

    public function zeroAction() {
        $products = $this->getDoctrine()->getRepository('WebSystemsRestBundle:Product')->findZero();
        if (empty($products)) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        }
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->createProductData($product);
        }
        return $this->createJsonResponse($data, JsonResponse::HTTP_OK);
    }

    public function postProductAction(Request $request)
    {
        $product = new Product();
        $name = $request->get('name');
        $amount = (int) $request->get('amount');
        if (empty($name) || $amount < 0) {
            return $this->createJsonResponse(self::MESSAGE_NOT_ACCEPTABLE, JsonResponse::HTTP_NOT_ACCEPTABLE);
        }
        $product->setName($name);
        $product->setAmount($amount);
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
        return $this->createJsonResponse(self::MESSAGE_ADDED, JsonResponse::HTTP_OK);
    }

    public function listProductAction()
    {
        $products = $this->getDoctrine()->getRepository('WebSystemsRestBundle:Product')->findAll();
        if (empty($products)) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        }
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->createProductData($product);;
        }
        return $this->createJsonResponse($data, JsonResponse::HTTP_OK);
    }

    public function putProductAction($id, Request $request)
    {
        $doctrine = $this->getDoctrine();
        $product = $doctrine->getRepository('WebSystemsRestBundle:Product')->findOneById($id);
        if (empty($product)) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        }
        $name = $request->get('name');
        $amount = (int) $request->get('amount');
        if (empty($name) || $amount < 0) {
            return $this->createJsonResponse(self::MESSAGE_NOT_ACCEPTABLE, JsonResponse::HTTP_NOT_ACCEPTABLE);
        }
        $product->setName($name);
        $product->setAmount($amount);
        $doctrine->getManager()->flush();
        return $this->createJsonResponse(self::MESSAGE_UPDATED, JsonResponse::HTTP_OK);
    }

    public function deleteProductAction($id)
    {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $product = $doctrine->getRepository('WebSystemsRestBundle:Product')->findOneById($id);
        if (empty($product)) {
            return $this->createJsonResponse(self::MESSAGE_NOT_FOUND, JsonResponse::HTTP_NOT_FOUND);
        } else {
            $em->remove($product);
            $em->flush();
        }
        return $this->createJsonResponse(self::MESSAGE_DELETED, JsonResponse::HTTP_OK);
    }

    private function createProductData($product)
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'amount' => $product->getAmount(),
        ];
    }

    private function createJsonResponse($data, $status) {
        return new JsonResponse([
            'result' => $data,
            'status' => $status
        ]);
    }
}