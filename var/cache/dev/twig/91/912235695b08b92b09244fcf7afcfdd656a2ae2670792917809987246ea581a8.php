<?php

/* product/index.html.twig */
class __TwigTemplate_a5ae5971980385e7ac56ba3823f5dee4d42f6b1b257a58b9e3aec4161bedd3b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>WebSystems - Client</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"/>
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\"/>
    <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\"/>
    <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
</head>
<body>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <button type=\"button\" class=\"btn btn-primary\" onclick=\"showModal();\">Add Product</button>
            </div>
            <div class=\"col\" id=\"optionPanel\">
                <div>
                    <input type=\"radio\" name=\"listOption\" checked=\"checked\" class=\"form-check-input\" id=\"all\">
                    <label class=\"form-check-label\" for=\"all\">Show all products</label>
                </div>
                <div>
                    <input type=\"radio\" name=\"listOption\" class=\"form-check-input\" id=\"five\">
                    <label class=\"form-check-label\" for=\"moreThanFive\">Show products with amount greater than 5</label>
                </div>
                <div>
                    <input type=\"radio\" name=\"listOption\" class=\"form-check-input\" id=\"zero\">
                    <label class=\"form-check-label\" for=\"zero\">Show products with amount equals 0</label>
                </div>
            </div>
        </div>
        <hr />
        <div class=\"row\">
            <div class=\"col\">
                <table id=\"data-table\" class=\"table\">
                    <thead class=\"thead-dark\">
                        <tr>
                            <th scope=\"col\">Id</th>
                            <th scope=\"col\">Product name</th>
                            <th scope=\"col\">Amount</th>
                            <th scope=\"col\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    ";
        // line 54
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4f406da_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_4f406da_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled/app_utils_1.js");
            // line 57
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "4f406da"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_4f406da") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled/app.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 59
        echo "
    <div class=\"modal fade\" id=\"productViewModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"productViewModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"productViewModalLabel\">Details</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group\">
                        <label for=\"vname\">Product name</label>
                        <div id=\"vname\"></div>
                    </div>
                    <div class=\"form-group\">
                        <label for=\"vamount\">Product amount</label>
                        <div id=\"vamount\"></div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"productModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"productModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"productModalLabel\">Edit/Add</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <form id=\"productForm\" onsubmit=\"return false;\">
                    <input type=\"hidden\" name=\"id\" id=\"id\">
                    <div class=\"modal-body\">
                        <div class=\"form-group\">
                            <label for=\"name\">Product name</label>
                            <input type=\"test\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Enter product name\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"amount\">Amount</label>
                            <input type=\"number\" class=\"form-control\" id=\"amount\" name=\"amount\" min=\"0\">
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" onclick=\"return false;\">Close</button>
                        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    ";
        // line 114
        $this->displayBlock('javascript', $context, $blocks);
        // line 181
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 114
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 115
        echo "    <script>
        \$(document).ready(function() {
            let dataTable = null;
            function init() {
                const options = \$('#optionPanel input[type=radio]');
                let optionType = 'all';
                \$(options).each(function() {
                   if (\$(this).is(':checked')) {
                       optionType = \$(this).attr('id');
                   }
                });
                if (optionType === 'all') {
                    optionType = '';
                }
                ajaxRequest().getAjax(0, optionType).done(function (data) {
                    const list = buildDataTables(data.result);
                    if (dataTable) {
                        dataTable.destroy();
                    }
                    dataTable = \$('#data-table').DataTable({
                        data: list
                    });
                });
            }
            \$('#productForm').on('submit', function() {
                const id = \$('#id').val();
                const form = \$(this).serialize();
                if (parseInt(id) > 0) {
                    ajaxRequest().putAjax(id, form).done(function (data) { });
                } else {
                    ajaxRequest().postAjax(form).done(function (data) { });
                }
                hideModal();
                init();
            });
            \$('#data-table').on('click', 'i.delete', function() {
                if (confirm('Are you sure, you want to delete this product?')) {
                    ajaxRequest().deleteAjax(\$(this).data('id')).done(function () {
                        init();
                    });
                }
            });

            \$('#optionPanel').on('click', 'input[type=radio]', function() {
                init();
            });

            \$('#moreThanFive').on('click', function() {
                if (\$(this).is(':checked')) {
                    ajaxRequest().getAjax(0, 'six').done(function(data) {
                        const list = buildDataTables(data.result);
                        if (dataTable) {
                            dataTable.destroy();
                        }
                        dataTable = \$('#data-table').DataTable({
                            data: list
                        });
                    })
                } else {
                    init();
                }
            })
            init();
        });
    </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 115,  172 => 114,  161 => 181,  159 => 114,  102 => 59,  88 => 57,  84 => 54,  36 => 9,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>WebSystems - Client</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"/>
    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.10/css/all.css\"/>
    <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\"/>
    <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
</head>
<body>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col\">
                <button type=\"button\" class=\"btn btn-primary\" onclick=\"showModal();\">Add Product</button>
            </div>
            <div class=\"col\" id=\"optionPanel\">
                <div>
                    <input type=\"radio\" name=\"listOption\" checked=\"checked\" class=\"form-check-input\" id=\"all\">
                    <label class=\"form-check-label\" for=\"all\">Show all products</label>
                </div>
                <div>
                    <input type=\"radio\" name=\"listOption\" class=\"form-check-input\" id=\"five\">
                    <label class=\"form-check-label\" for=\"moreThanFive\">Show products with amount greater than 5</label>
                </div>
                <div>
                    <input type=\"radio\" name=\"listOption\" class=\"form-check-input\" id=\"zero\">
                    <label class=\"form-check-label\" for=\"zero\">Show products with amount equals 0</label>
                </div>
            </div>
        </div>
        <hr />
        <div class=\"row\">
            <div class=\"col\">
                <table id=\"data-table\" class=\"table\">
                    <thead class=\"thead-dark\">
                        <tr>
                            <th scope=\"col\">Id</th>
                            <th scope=\"col\">Product name</th>
                            <th scope=\"col\">Amount</th>
                            <th scope=\"col\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {% javascripts
        'js/utils.js'
        output='js/compiled/app.js' %}
        <script src=\"{{ asset_url }}\"></script>
    {% endjavascripts %}

    <div class=\"modal fade\" id=\"productViewModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"productViewModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"productViewModalLabel\">Details</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group\">
                        <label for=\"vname\">Product name</label>
                        <div id=\"vname\"></div>
                    </div>
                    <div class=\"form-group\">
                        <label for=\"vamount\">Product amount</label>
                        <div id=\"vamount\"></div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"productModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"productModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"productModalLabel\">Edit/Add</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <form id=\"productForm\" onsubmit=\"return false;\">
                    <input type=\"hidden\" name=\"id\" id=\"id\">
                    <div class=\"modal-body\">
                        <div class=\"form-group\">
                            <label for=\"name\">Product name</label>
                            <input type=\"test\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Enter product name\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"amount\">Amount</label>
                            <input type=\"number\" class=\"form-control\" id=\"amount\" name=\"amount\" min=\"0\">
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" onclick=\"return false;\">Close</button>
                        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {% block javascript %}
    <script>
        \$(document).ready(function() {
            let dataTable = null;
            function init() {
                const options = \$('#optionPanel input[type=radio]');
                let optionType = 'all';
                \$(options).each(function() {
                   if (\$(this).is(':checked')) {
                       optionType = \$(this).attr('id');
                   }
                });
                if (optionType === 'all') {
                    optionType = '';
                }
                ajaxRequest().getAjax(0, optionType).done(function (data) {
                    const list = buildDataTables(data.result);
                    if (dataTable) {
                        dataTable.destroy();
                    }
                    dataTable = \$('#data-table').DataTable({
                        data: list
                    });
                });
            }
            \$('#productForm').on('submit', function() {
                const id = \$('#id').val();
                const form = \$(this).serialize();
                if (parseInt(id) > 0) {
                    ajaxRequest().putAjax(id, form).done(function (data) { });
                } else {
                    ajaxRequest().postAjax(form).done(function (data) { });
                }
                hideModal();
                init();
            });
            \$('#data-table').on('click', 'i.delete', function() {
                if (confirm('Are you sure, you want to delete this product?')) {
                    ajaxRequest().deleteAjax(\$(this).data('id')).done(function () {
                        init();
                    });
                }
            });

            \$('#optionPanel').on('click', 'input[type=radio]', function() {
                init();
            });

            \$('#moreThanFive').on('click', function() {
                if (\$(this).is(':checked')) {
                    ajaxRequest().getAjax(0, 'six').done(function(data) {
                        const list = buildDataTables(data.result);
                        if (dataTable) {
                            dataTable.destroy();
                        }
                        dataTable = \$('#data-table').DataTable({
                            data: list
                        });
                    })
                } else {
                    init();
                }
            })
            init();
        });
    </script>
    {%  endblock %}
</body>
</html>", "product/index.html.twig", "F:\\symfony\\websystems\\app\\Resources\\views\\product\\index.html.twig");
    }
}
