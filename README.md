# Used technology:
- Symfony 3
- PHP 5.6
- JQuery
- JavaScript
- DataTables
- MySql


# Installation

DataBase

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product` (`id`, `name`, `amount`)
VALUES  (1, 'Produkt 1', 4),
	(2, 'Produkt 2', 12),
	(3, 'Produkt 5', 0),
	(4, 'Produkt 7', 6),
	(5, 'Produkt 8', 2),

------------

composer.json
- run this commands
	- composer require friendsofsymfony/rest-bundle
	- composer require jms/serializer-bundle
	- composer require symfony/assetic-bundle
- in section autoload  psr-4 add as following
	- "WebSystems\\RestBundle\\": "src/WebSystems/RestBundle"
	- "WebSystems\\ClientBundle\\": "src/WebSystems/ClientBundle"
- run command composer dump-autoload to generate autoload files

------------

config.yml
- in section framework add
 	  templating:
     	   engines: ['twig']
- add this code at the end of file:
		fos_rest:
    		view:
        		formats:
            		json: true
    		routing_loader:
                default_format: json
      		format_listener:
        		rules:
            		- { path: '^/api/', priorities: ['json'], prefer_extension: false }
            		- { path: '^/', priorities: [ 'html', '*/*'], fallback_format: html, prefer_extension: false }

		assetic:
    		debug:          '%kernel.debug%'
    		use_controller: '%kernel.debug%'
    		filters:
        		cssrewrite: ~

------------

routing.yml
- Add as following:
		web_systems_client:
    		resource: "@WebSystemsClientBundle/Controller/ProductsController.php"
    		type: annotation

		web_systems_rest:
    		resource: "@WebSystemsRestBundle/Controller/ProductsController.php"
    		prefix:   /api/
    		type: rest

------------

AppKernel.php

-  public function registerBundles()
        $bundles = [
            ....
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new WebSystems\RestBundle\WebSystemsRestBundle(),
            new WebSystems\ClientBundle\WebSystemsClientBundle()
            ....
        ];

------------

At the end, start the command  php bin/console asset:dump

From this moment project should works. It's available on link {HOST}/product.